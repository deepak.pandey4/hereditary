// Modal Up & Down 
 
$(document).ready(function () {
    $(".show-modal").click(function () {
        $("#myModal").modal({
            backdrop: 'static',
            keyboard: false
        });
    });
});


// $(".modal").keydown(function (e) {
//     if (e.keyCode == 40) { $('.modal-body').scrollTop($('.modal-body').scrollTop() + 20); }
//     if (e.keyCode == 38) { $('.modal-body').scrollTop($('.modal-body').scrollTop() - 20); }
// });

 


// Carousel Home page
$('.carousel').carousel({
    interval: 2000
})

$(document).ready(function () {
        setTimeout(function () {
              $('body').addClass('loaded');  
        }, 5000);    
});


// gateway ancestor - First remove from that page then un-comment this one 
// $(document).ready(function () {


//   $(document).ready(function () {


//       var slides = $('.slide');
//       $('.slider-for').slick({

//           slidesToShow: 1,
//           slidesToScroll: 1,
//           arrows: false,
//           fade: false,
//           asNavFor: '.slider-nav'
//       }).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
//           slides.removeClass('fadeIn');
//           slides.eq(nextSlide).addClass('fadeIn');
//           slides.eq(nextSlide + 1).addClass('fadeIn');
//       });
//       $('.slider-nav').slick({

//           slidesToShow: 7,
//           slidesToScroll: 1,
//           asNavFor: '.slider-for',
//           prevArrow: '<img src="./images/back.png" class="prev img-fluid">',
//           nextArrow: '<img src="./images/next.png" class="next img-fluid">',
//           dots: false,
//           centerMode: true,
//           focusOnSelect: true,
//           vertical: false,
//           verticalSwiping: false,
//           responsive: [{
//                   breakpoint: 993,
//                   settings: {
//                       arrows: false,
//                       centerMode: true,
//                       centerPadding: '40px',
//                       slidesToShow: 5
//                   }
//               },
//               {
//                   breakpoint: 480,
//                   settings: {
//                       arrows: false,
//                       centerMode: false,
//                       // centerPadding: '40px',
//                       slidesToShow: 2
//                   }
//               }
//           ]

//       });


//   });

// ========================= To make div or section fade in Bottom==================================
 
// Intersection Oberver 
"use strict";

// Update entry
var updateSection = function updateSection(entry) {
    if (entry.isIntersecting) {
        entry.target.classList.add('article__section--active');
    }
}; // Handle callback


var intersectCallback = function intersectCallback(entries, observer) {
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
        for (var _iterator = entries[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var entry = _step.value;
            updateSection(entry);
        }
    } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion && _iterator.return != null) {
                _iterator.return();
            }
        } finally {
            if (_didIteratorError) {
                throw _iteratorError;
            }
        }
    }
}; // Create Observer


var options = {
    root: null,
    rootMargin: '0px',
    threshold: 0.1
};
var observer = new IntersectionObserver(intersectCallback, options); // Observe sections

var sections = document.getElementsByClassName('js-article-section');
var _iteratorNormalCompletion2 = true;
var _didIteratorError2 = false;
var _iteratorError2 = undefined;

try {
    for (var _iterator2 = sections[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
        var section = _step2.value;
        observer.observe(section);
    }
} catch (err) {
    _didIteratorError2 = true;
    _iteratorError2 = err;
} finally {
    try {
        if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
            _iterator2.return();
        }
    } finally {
        if (_didIteratorError2) {
            throw _iteratorError2;
        }
    }
}
/*==============================================================*/ 

// ===================================================================
                
                // SideBar Navigation
                
         $(document).ready(function () {
             $('.collapse').on('show.bs.collapse', function () {
                 $('.collapse.show').each(function () {
                     $(this).collapse('hide');
                 });
             });
         });

// ===================================================================


// =========================================================================
            // Scroll Top Function
$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});
// =========================================================================

 