exports.sortedByListYearcronological = (li) => {
  li.sort((a, b) => {
    return a.listYear - b.listYear;
  });
};

exports.sortedByListYearinsigniaDecoration = (li) => {
  console.log(li)
  li.sort((a, b) => {
    return a.Year - b.Year;
  });
};

exports.sortedByListYearInsigniaPrecedence = (li) => {
  li.sort((a, b) => {
    return a.listYear - b.listYear;
  });
};


exports.sortedByListNameOnline = (li) => {
    li.sort((a, b) => {
        if (a.listName < b.listName)
        return -1;
      if (a.listName > b.listName)
        return 1;
      return 0;
    });
  };
  exports.sortedByListNameAlpha = (li) => {
    li.sort((a, b) => {
        if (a.listName < b.listName)
        return -1;
      if (a.listName > b.listName)
        return 1;
      return 0;
    });
  };

  exports.sortedByfamilyTitle = (li) => {
    li.sort((a, b) => {
        if (a.familyTitle < b.familyTitle)
        return -1;
      if (a.familyTitle > b.familyTitle)
        return 1;
      return 0;
    });
  };

  exports.sortedBycertificateName = (li) => {
    li.sort((a, b) => {
        if (a.certificateName < b.certificateName)
        return -1;
      if (a.certificateName > b.certificateName)
        return 1;
      return 0;
    });
  };

  exports.sortedByListName = (li) => {
    li.sort((a, b) => {
        if (a.name < b.name)
        return -1;
      if (a.name > b.name)
        return 1;
      return 0;
    });
  };

  exports.sortResumeByName = (li)=>{
   li.sort((a,b)=>{
     if(a.resumeName < b.resumeName)
       return -1
     if(a.resumeName > b.resumeName)
       return 1;
     return 0
   })
 }

  