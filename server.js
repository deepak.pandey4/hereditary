const express = require('express');
const path = require('path');
const cors = require('cors');
require('dotenv').config();
const port = process.env.PORT||2000
const viewRouter = require('./router/viewRouter');
const staticPath = path.join(__dirname, "./public/")
const app = express();

app.use(cors({
  "Access-Control-Allow-Origin": "*",
  credentials:true
}));
app.use(express.static(staticPath));
app.set('view engine', "ejs");
app.use('/', viewRouter);



app.listen(port,()=>{
  console.log(`Server is live at ${port}`)
})




