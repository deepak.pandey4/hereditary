const express = require('express');
const router = require('express').Router();
const axios = require('axios');
const sorting = require('../helper/filter')




router.get('/', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const home = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/home/getHomeData/61dd504d4ee74e7bcc5242eb`);
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const Data = home.data.findRes;
        const para = footer.data.findRes;
        const nav = header.data.findRes;
        res.render('index', { Data, para,nav})
    } catch (error) {
        console.log("data not exist")
    }
})
router.get('/overview', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const overview = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/overview/getOverViewData/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const overviewdata = (overview.data.findRes);
        const para = footer.data.findRes;
        const nav = header.data.findRes;
        res.render('overview', { overviewdata, para,nav })
    } catch (error) {
        console.log("data not exist")
    }
})

router.get('/members_annual', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const memberAnual = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/members_annual/getAll_members_annual_DataPage/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const member = memberAnual.data.findRes;
        const para = footer.data.findRes;
        const nav = header.data.findRes;
        res.render('members_annual', { member, para,nav })
    } catch (error) {
        console.log("data not exist")
    }

})

router.get('/directory-alphabetical', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const directoryAlp = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/directory_alphabetical/getAll_directory_alphabetical_Data/61dd504d4ee74e7bcc5242eb`)
        const  directory_list = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/directory_list/getAllList/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = directoryAlp.data.findRes;
        const list = directory_list.data.findRes.list;
        sorting.sortedByListNameAlpha(list)
        const para = footer.data.findRes;
        const nav = header.data.findRes;
        res.render('directory-alphabetical', { data, para,list,nav })
    } catch (error) {
        console.log("data not found")
    }
})

router.get('/directory-online', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const directoryOn = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/directory_online/getDirectory_online_Data/61dd504d4ee74e7bcc5242eb`)
        const  directory_list = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/directory_list/getAllList/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = directoryOn.data.findRes;
        const list = directory_list.data.findRes.list;
        sorting.sortedByListNameOnline(list)
        const para = footer.data.findRes;
        const nav = header.data.findRes;
        res.render('directory-online', { data, para, list,nav})
    } catch (error) {
        console.log("data not found")
    }
})


router.get('/directory-cronological', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const directoryCronological = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/directory_cronological/getDirectory_cronologicalAllData/61dd504d4ee74e7bcc5242eb`)
        const  directory_list = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/directory_list/getAllList/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = directoryCronological.data.findRes;
        const list = directory_list.data.findRes.list;
        sorting.sortedByListYearcronological(list)
        const para = footer.data.findRes;
        const nav = header.data.findRes;
        res.render('directory-cronological', { data, para, list,nav })
    } catch (error) {
        console.log(error)
    }
})


router.get('/heraldary-overview', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const heraldaryOverview = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/heraldary_overview/get_all_heraldary_overview_Data/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = heraldaryOverview.data.findRes;
        const para = footer.data.findRes;
        const nav = header.data.findRes;
        res.render('heraldary-overview', { data, para, nav})
    } catch (error) {
        console.log(error)
    }
})

router.get('/insignia-decoration', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const insigniaDecorations = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/insignia_decoration/getAllData_to_insignia_decoration_page/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = insigniaDecorations.data.findRes
        sorting.sortedByListYearinsigniaDecoration(data.insignia_decoration_medalList)
        const para = footer.data.findRes
        const nav = header.data.findRes
        res.render('insignia-decoration',{data,para,nav})
    } catch (error) {
        console.log(error)
    }
})

router.get('/directory-submission', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const directorySubmission = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/directory_submission/getAllPage_Data_Of_directory_submission/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = directorySubmission.data.findRes
        const para = footer.data.findRes;
        const nav = header.data.findRes;
        res.render('directory-submission',{para,data,nav})
    } catch (error) {
       console.log(error) 
    }
})

router.get('/search', async(req, res) => {
        try {
            const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
            const searchPage = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/search/getAllData_from_searchPage/61dd504d4ee74e7bcc5242eb`)
            const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
            const para = footer.data.findRes;
            const search = searchPage.data.findRes;
            const nav = header.data.findRes;
            res.render('search', { search, para,nav })
        } catch (error) {
            console.log("data not found")
        }
    })
    // this is end of search page

router.get('/family-associations', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const familyAssociation = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/family_associations/getFamily_associationPageData/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const para = footer.data.findRes;
        const data = familyAssociation.data.findRes;
        sorting.sortedByfamilyTitle(data.family_associationList)
        const nav = header.data.findRes;
        res.render('family-associations', { data, para, nav})
    } catch (error) {
        console.log(error)
    }
})

router.get('/our-mission', async(req, res) => {
        try {
            const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
            const ourMission = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/our_mission/getOur_mission_PageData/61dd504d4ee74e7bcc5242eb`)
            const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
            const para = footer.data.findRes;
            const our = ourMission.data.findRes;
            const nav = header.data.findRes;
            res.render('our-mission', { our, para,nav })
        } catch (error) {
            console.log(error)
        }
    })
    // our_mission is panding we are section list

router.get('/certificate', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const certificate = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/certificate/getCertificatePageData/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const para = footer.data.findRes;
        const data = certificate.data.findRes;
        sorting.sortedBycertificateName(data.certificateOverviewList)
        const nav = header.data.findRes;
        res.render('certificate', { data, para,nav })
    } catch (error) {
        console.log(error)
    }
})

router.get('/research', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const research = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/research/getResearchPageData/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = research.data.findRes;
        const para = footer.data.findRes;
        const nav = header.data.findRes;
        res.render('research', { data, para, nav })
    } catch (error) {
        console.log(error)
    }
})

router.get('/gateway-ancestor', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const gatewayAncestor = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/gateway_ancestor/getAllPage_gateway_ancestorData/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = gatewayAncestor.data.findRes;
        sorting.sortedByListName(data.gateway_ancestorList)
        const para = footer.data.findRes;
        const nav = header.data.findRes;
        res.render('gateway-ancestor', { data, para,nav })
    } catch (error) {
        console.log(error)
    }
})


router.get('/chivalric-studies', async(req, res) => {
   try {
    const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
     const chivalricStudies = await axios(`https://hereditary-backend.herokuapp.com/api/v2/chivalric_studies/getAllData_from_chivalric_studiesPage/61dd504d4ee74e7bcc5242eb`)
     const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
     const data = chivalricStudies.data.findRes;
     const para = footer.data.findRes;
     const nav = header.data.findRes;
     res.render('chivalric-studies',{data,para,nav})
    } catch (error) {
        console.log(error)
    }
})


router.get('/calendar', async(req, res) => {
   try {
    const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
       const calendar = await axios(`https://hereditary-backend.herokuapp.com/api/v2/calendar/getAllData_from_calendarPage/61dd504d4ee74e7bcc5242eb`)
       const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
       const data = calendar.data.findRes;
       const para = footer.data.findRes;
       const nav = header.data.findRes;
       res.render('calendar',{data,para,nav})
   } catch (error) {
       console.log(error)
   }
})


router.get('/annual-reception',async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const annualReception = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/annual_reception/getAllData_from_annual_receptionPage/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = annualReception.data.findRes;
        const para = footer.data.findRes;
        const nav = header.data.findRes;
        res.render('annual-reception',{para,data,nav})
    } catch (error) {
       console.log(error) 
    }
})

router.get('/members_alphabetical', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const membersAlphabetical = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/members_alphabetical/get_all_members_alphabeticalPageData/61dd504d4ee74e7bcc5242eb`)
        const resumlist = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/resume/getAllResume/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = membersAlphabetical.data.findRes[0];
        const para = footer.data.findRes;
        const list = resumlist.data.result[0].resumes
        sorting.sortResumeByName(list)
        const nav = header.data.findRes;
        res.render('members_alphabetical',{para,data,list,nav})
    } catch (error) {
       console.log(error) 
    }
})

router.get('/patronage-patrons', async(req, res) => {
    try {
    const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
    const patronagePatrons = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/patronage_patrons/getAllData_from_patronage_patronsPage/61dd504d4ee74e7bcc5242eb`)
    const resumlist = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/resume/getAllResume/61dd504d4ee74e7bcc5242eb`)
    const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
    const data = patronagePatrons.data.findRes;
    const para = footer.data.findRes;
    const list = resumlist.data.result[0].resumes;
    const nav = header.data.findRes;
    res.render('patronage-patrons',{data,para,list,nav})
    } catch (error) {
        console.log(error)
    }
})

router.get('/patronage-partner-organisation', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const patronagePartnerOrganisation = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/patronage_partner_organisation/getAllData_from_patronage_partner_organisationPage/61dd504d4ee74e7bcc5242eb`)
        const  directory_list = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/directory_list/getAllList/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = patronagePartnerOrganisation.data.findRes;
        const list = directory_list.data.findRes.list;
        const para = footer.data.findRes;
        const nav = header.data.findRes;
       res.render('patronage-partner-organisation',{data,para,list,nav})
    } catch (error) {
        console.log(error)
    }
   
})

router.get('/project', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const project = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/project/getAllData_from_projectPage/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = project.data.findRes;
        const para = footer.data.findRes;
        const nav = header.data.findRes;
        res.render('project',{data,para,nav}) 
    } catch (error) {
        console.log(error)
    }
})

router.get('/leadership-advisory-council', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const leadershipAdvisoryCouncil = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/leadership_advisory_council/getAllPage_Data_Of_leadership_advisory_council/61dd504d4ee74e7bcc5242eb`)
        const resumlist = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/resume/getAllResume/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = leadershipAdvisoryCouncil.data.findRes;
        const list = resumlist.data.result[0].resumes
        const para = footer.data.findRes;
        const nav = header.data.findRes;
        res.render('leadership-advisory-council',{data,para,list,nav})
    } catch (error) {
        console.log(error)
    }
   
})


router.get('/leadership-advisory-conultant', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const leadershipAdvisoryConultant = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/leadership_advisory_conultant/getAllPage_Data_Of_leadership_advisory_conultant/61dd504d4ee74e7bcc5242eb`)
        const resumlist = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/resume/getAllResume/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = leadershipAdvisoryConultant.data.findRes;
        const list = resumlist.data.result[0].resumes
        const para = footer.data.findRes;
        const nav = header.data.findRes;
        res.render('leadership-advisory-conultant',{data,para,list,nav})
    } catch (error) {
      console.log(error)  
    }
})

router.get('/gavel', async(req, res) => {
 try {
    const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
     const gavel = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/gavel/getAllPage_Data_Of_gavel/61dd504d4ee74e7bcc5242eb`)
     const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
     const data = gavel.data.findRes;
     const para = footer.data.findRes;
     const nav = header.data.findRes;
     res.render('gavel',{data,para,nav})
 } catch (error) {
     console.log(error)
 }

})

router.get('/insignia-precedence', async(req, res) => {
    try {
    const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
    const insigniaPrecedence = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/insignia_precedence/getAllData_to_insignia_precedence_page/61dd504d4ee74e7bcc5242eb`)
    const  directory_list = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/directory_list/getAllList/61dd504d4ee74e7bcc5242eb`)
    const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
    const data = insigniaPrecedence.data.findRes;
    const list = directory_list.data.findRes.list;
    sorting.sortedByListYearInsigniaPrecedence(list)
    const para = footer.data.findRes;
    const nav = header.data.findRes;
    res.render('insignia-precedence',{para,data,list,nav})
    } catch (error) {
        console.log(error)
    }
    
})



router.get('/insignia-regulations_gentlemen', async(req, res) => {
   try {
       const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
       const insigniaRegulationsGentlemen = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/insignia_regulations_gentlemen/getAllData_of_insignia_regulations_gentlemen_page/61dd504d4ee74e7bcc5242eb`)
       const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
       const data = insigniaRegulationsGentlemen.data.findRes;
       const para = footer.data.findRes;
       const nav = header.data.findRes;
    res.render('insignia-regulations_gentlemen',{data,para,nav}) 
    } catch (error) {
        console.log(error)
    }
})


router.get('/insignia-regulations_ladies', async(req, res) => {
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const insigniaRegulationsLadies = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/insignia_regulations_ladies/getAllData_of_insignia_regulations_ladies_page/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = insigniaRegulationsLadies.data.findRes;
        const para = footer.data.findRes;
        const nav = header.data.findRes;
        res.render('insignia-regulations_ladies',{data,para,nav})
    } catch (error) {
        console.log(error)
    }
    
})
router.get('/members_details', async(req,res)=>{
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const resumeData = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/resume/getResumeData/${req.query.id}`)
        const membersDetails = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/members_details/getAllMembers_detailsPageData/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = membersDetails.data.findRes;
        const para = footer.data.findRes;
        const bioData = resumeData.data.findRes[0].resumes
        const nav = header.data.findRes;
        res.render('members_details',{para,data,bioData,nav})
    } catch (error) {
        console.log(error)
    }
})

router.get('/members', async(req,res)=>{
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const resumesData = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/resume/getResumeByClassId/${req.query.classofId}`)
        const members = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/members/getAllMembersPageData/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const data = members.data.findRes;
        const para = footer.data.findRes;
        const bioData = resumesData.data.findRes[0].resumes
        const nav = header.data.findRes;
        res.render('members',{para,data,bioData,nav})
    } catch (error) {
        
    }
})



router.get('/contact-us', async(req, res) => {
     try {
      const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
      const contactUs = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/contact_us/getContactUsPageAllData/61dd504d4ee74e7bcc5242eb`)
      const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
      const data = contactUs.data.findRes;
      const para = footer.data.findRes;
      const nav = header.data.findRes;
      res.render('contact-us',{data,para,nav})
    } catch (error) {
        console.log(error)
    }
})

router.get('/:slugName', async(req,res)=>{
    try {
        const header = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/nav/getAllNavbarData/61dd504d4ee74e7bcc5242eb`)
        const footer = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/footer/getfooterData/61dd504d4ee74e7bcc5242eb`)
        const profileData = await axios.get(`https://hereditary-backend.herokuapp.com/api/v2/resume/getResumeBySlugName/${req.params.slugName}`)
        const data = profileData.data.findRes[0].resumes[0]
        const para = footer.data.findRes;
         const nav = header.data.findRes;
        res.render('profile',{data,nav,para})
    } catch (error) {
        console.log(error)
    }
})




module.exports = router;